# arch-linux-tweak-tool

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![crates.io](https://img.shields.io/crates/v/arch-linux-tweak-tool.svg)](https://crates.io/crates/arch_linux_tweak_tool)
[![docs.rs](https://img.shields.io/docsrs/arch-linux-tweak-tool)](https://docs.rs/arch_linux_tweak_tool)
[![crates.io](https://img.shields.io/crates/d/arch-linux-tweak-tool.svg)](https://crates.io/crates/arch_linux_tweak_tool)

A rust based tweak tool for the arch linux power user

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Development](#development)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background

This project is inspired by all the hard work done by [Erik Dubois](https://github.com/erikdubois)
on the original [archlinux-tweak-tool](https://github.com/arcolinux/archlinux-tweak-tool) for [Arco Linux](https://arcolinux.com/).
I have felt the tool could use a redesign and more features added at supporting more tooling, and other Arch linux spins.
The original tool is written in Python and Gtk3, a GUI toolkit I find ugly and old.
This version is written in Rust using the [Pure Rust Iced GUI toolit](https://github.com/iced-rs/).

## Install

```
TODO
```

## Usage

```
TODO
```

## [Development]

Only requires `just` to bootstrap all tools and configuration.

```bash
cargo install just
just init # setup repo, install hooks and all required tools
```

To run:

```bash
just run
```

To test:

```bash
just test
```

Before committing your work:

```bash
just pre-commit
```

To see all available commands:

```bash
just list
```

## Maintainers

[@thebashpotato](https://gitlab.com/thebashpotato)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

This project is licensed under the [AGPL](./LICENSE) © 2023 Matt Williams
